
#ifndef UCSD148_H
#define UCSD148_H

#define PASS(x) __asm__ volatile ("mtc0 %0, $23" : : "r" ((x)))
#define FAIL(x) __asm__ volatile ("mtc0 %0, $24" : : "r" ((x)))
#define DONE(x) __asm__ volatile ("mtc0 %0, $25" : : "r" ((x)))

#endif
