/* 
 * Taken from the Computer Languge Benchmarks game and revised by Max Morehead
 * for UCSD CSE 148. The Computer Language Benchmarks game provides files under
 * a BSD license (reproduced at bottom of program).
 *
 * The Computer Language Benchmarks Game
 * https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
 *
 * contributed by Ledrug Katz
 *
 */


#include "ucsd148.h"

/* this depends highly on the platform.  It might be faster to use
    char type on 32-bit systems; it might be faster to use unsigned. */

typedef int elem;

elem s[16], t[16];

int maxflips = 0;
int max_n;
int odd = 0;
int checksum = 0;

int flip()
{
    register int i;
    register elem *x, *y, c;

    for (x = t, y = s, i = max_n; i--; )
        *x++ = *y++;
    i = 1;
    do {
        for (x = t, y = t + t[0]; x < y; )
            c = *x, *x++ = *y, *y-- = c;
        i++;
    } while (t[t[0]]);
    return i;
}

inline void rotate(int n)
{
    elem c;
    register int i;
    c = s[0];
    for (i = 1; i <= n; i++) s[i-1] = s[i];
    s[n] = c;
}

/* Tompkin-Paige iterative perm generation */
void tk(int n)
{
    int i = 0, f;
    elem c[16] = {0};

    while (i < n) {
        rotate(i);
        if (c[i] >= i) {
            c[i++] = 0;
            continue;
        }

        c[i]++;
        i = 1;
        odd = ~odd;
        if (*s) {
            f = s[s[0]] ? flip() : 1;
            if (f > maxflips) maxflips = f;
            checksum += odd ? -f : f;
        }
    }
}

int main()
{
    int i;


    max_n = 8;

    for (i = 0; i < max_n; i++) s[i] = i;
    tk(max_n);

    if (checksum == 1616) {
        PASS(checksum);
    } else {
        FAIL(checksum);
    }

    if (maxflips == 22) {
        PASS(maxflips);
    } else {
        FAIL(maxflips);
    }

    return 0;
}

/*

Revised BSD license

This is a specific instance of the Open Source Initiative (OSI) BSD license template.

Copyright © 2004-2008 Brent Fulgham, 2005-2022 Isaac Gouy

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

     Neither the name "The Computer Language Benchmarks Game" nor the name "The Benchmarks Game" nor the name "The Computer Language Shootout Benchmarks" nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
