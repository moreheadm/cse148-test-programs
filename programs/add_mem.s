
    .text
    .abicalls
    .p2align 2
    .type main,@function

    .set noreorder
    .set nomacro

    .globl main
    .ent main

main:

    lui $v0, %hi(in_data)
    addiu $v1, $v0, 4
    lw $t0, %lo(in_data)($v0)
    lw $t1, %lo(in_data)($v1)

    add $t2, $t0, $t1
    mtc0 $t2, $23 # 55

    lui $t9, %hi(result)
    addiu $t9, $t9, %lo(result)

    sw $t2, 0($t9)
    lw $t3, 0($t9)
    mtc0 $t3, $23 # 55

    addiu $v1, $v1, 4
    lw $t4, %lo(in_data)($v1)

    add $t3, $t3, $t4
    mtc0 $t3, $23 # 35
    sw $t3, 0($t9)
    lw $t2, 0($t9)
    mtc0 $t2, $23 # 35

    addiu $v1, $v1, 4
    lw $t5, %lo(in_data)($v1)
    add $t2, $t2, $t5 # 30
    mtc0 $t2, $23 

    sw $t2, 0($t9)
    lw $t0, 0($t9)
    
    mtc0 $t0, $25 #30
    nop
repeat:
    j repeat
    nop


    .data
    .p2align 2
in_data:
    .4byte 33
    .4byte 22
    .4byte -20
    .4byte -5
    .size in_data, 20

    .p2align 2
result:
    .byte 0

    .size result, 4
