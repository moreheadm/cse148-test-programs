
    .text
    .abicalls
    .p2align 2
    .type main,@function

    .set noreorder
    .set nomacro

    .globl main
    .ent main

main:
    li $t0, 5
    li $t1, -10
    
    add $t2, $t1, $t0
    bltz $t2, branch1
    addi $t2, $t2, 10
    addi $t2, $t2, -10
    j   end1
    nop
branch1:
    nop
end1:
    
    bltz $t2, branch2
    nop
    mtc0 $t2, $23
    j end2
    nop
branch2:
    mtc0 $t2, $24
end2:
    jr $ra
    nop
