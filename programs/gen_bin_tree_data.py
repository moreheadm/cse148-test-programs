
import random

def to_c_list(L, max_line_length=100):
    str_L = [str(x) + ', ' for x in L]
    str_L[0] = '{' + str_L[0]
    str_L[-1] = str_L[-1][:-2] + '}'

    lines = ['']
    for s in str_L:
        if (len(lines[-1]) + len(s) <= max_line_length):
            lines[-1] = lines[-1] + s
        else:
            lines[-1] = lines[-1][:-1] # remove space
            lines.append(s)

    return '\n'.join(lines)
    
n1 = 5000
n2 = 5000

keys = []
values = []
QD = []

for i in range(n1):
    keys.append(random.randint(0, 1000000))
    values.append(random.randint(0, 1000))

for i in range(n2):
    if (random.random() < 0.5):
        QD.append(keys[random.randint(0, n1 - 1)])
    else:
        QD.append(random.randint(0, 1000000))

print(to_c_list(keys) + '\n\n\n')
print(to_c_list(values) + '\n\n\n')
print(to_c_list(QD))
