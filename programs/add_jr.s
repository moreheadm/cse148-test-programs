
    .text
    .abicalls
    .p2align 2
    .type main,@function

    .set noreorder
    .set nomacro

    .globl main
    .ent main

main:
    li $t0, 5
    li $t1, -10
    li $t2, 6
    li $t3, 20
    li $t4, 25
    
    add $t5, $t0, $t1 # -5
    mtc0 $t5, $23 # t7 = -5
    add $t6, $t5, $t2 # 1
    mtc0 $t6, $23 # t7 = 1
    add $t5, $t3, $t4 # 45
    mtc0 $t5, $23 # t7 = 45
    add $t6, $t5, $t6 # 46
    mtc0 $t6, $23 # t7 = 46
    add $t7, $t6, $t0 # 51

    jr $ra
    nop
repeat:
    j repeat
    nop
