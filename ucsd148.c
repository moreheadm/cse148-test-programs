
#include "ucsd148.h"

void * memcpy(void * restrict dest, const void * restrict src, unsigned int n) {
    if (n & 3) FAIL(0x12345678);

    int * d = dest;
    const int * s = src;

    for (unsigned int i = 0; i < (n >> 2); i++) {
        d[i] = s[i];
    }

    return dest;
}


void * memset(void * s, int c, unsigned int n) {
    if (n & 3) FAIL(0x12345678);
    int * int_s = s;

    int c4 = c + (c << 8) + (c << 16) + (c << 24);

    for (unsigned int i = 0; i < (n >> 2); i++) {
        int_s[i] = c4;
    }

    return s;
}

void * memmove(void * dest, const void * src, unsigned int n) {
    if (n & 3) FAIL(0x12345678);

    int * d = dest;
    const int * s = src;

    if (src == dest) { 
        return dest;
    } else if (src < dest) {
        for (unsigned int i = (n >> 2) - 1; i >= 0; i++) {
            d[i] = s[i];
        }
    } else {
        for (unsigned int i = 0; i < (n >> 2); i++) {
            d[i] = s[i];
        }
    }

    return dest;
}
