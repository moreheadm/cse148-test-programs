#!/bin/sh

BU="$CSE148_TOOLS/binutils/bin"
SDIR=$(dirname "${BASH_SOURCE[0]:-$0}") # script directory

FILENAME="${1%.*}"
EXTENSION="${1#$FILENAME}"
EXTENSION="${EXTENSION#.}"

if [ "$EXTENSION" != "c" ] && [ "$EXTENSION" != "s" ]; then
    echo "Command must have argument with extension .c or .s"
    exit 1
fi

clang --target=mips -mcpu=mips2 -O2 -c "$1" -o "${FILENAME}.o" -I"$SDIR" -ffreestanding
clang --target=mips -mcpu=mips2 -c "$SDIR/start.s" -o "$SDIR/start.o"
clang --target=mips -mcpu=mips2 -O2 -c "$SDIR/ucsd148.c" -o "$SDIR/ucsd148.o" -ffreestanding -ffunction-sections -fdata-sections

"$BU/mips-ld" --gc-sections -T "$SDIR/ucsd148.ls" "$SDIR/start.o" "$SDIR/ucsd148.o" "${FILENAME}.o" -o "${FILENAME}.bin"
"$BU/mips-objdump" -D "${FILENAME}.bin" > "${FILENAME}.dis"
"$BU/mips-objcopy" -O binary "${FILENAME}.bin" "${FILENAME}.oc"
xxd -p -c4 "${FILENAME}.oc" "${FILENAME}.hex"

rm -f "${FILENAME}.bin"
rm -f "${FILENAME}.o"
rm -f "${FILENAME}.oc"
